package practica1;

public class mainProgrammer {

	public static void main(String[] args) {
		Programmer programador1 = new Programmer();
		System.out.println("Programador 1");
		System.out.println("-------------");
		System.out.println("ID: " + programador1.id + "\nNom: " + programador1.name + "\nSalari: "
				+ programador1.salary);
		System.out.println();
		Programmer programador2 = new Programmer("Alejandro", 2, 1500);
		System.out.println("Programador 2");
		System.out.println("-------------");
		programador2.id = 5;
		programador2.salary = 2000;
		System.out.println("ID: " + programador2.id + "\nNom: " + programador2.name + "\nSalari: "
				+ programador2.salary);
	}

}
