package practica1;

public class Programmer {
	String name;
	int id;
	int salary;

	public Programmer() {
		this.name = "John";
		this.id = 1;
		this.salary = 1000;
	}

	public Programmer(String name, int id, int salary) {
		this.name = name;
		this.id = id;
		this.salary = salary;
	}
}
