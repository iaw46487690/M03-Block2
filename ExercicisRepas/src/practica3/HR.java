package practica3;

public class HR extends Employee {

	public HR(int employeeId, String name, int age) {
		super(employeeId, name, age);
	}

	int monthlyWage = 2300;
	int monthsWorked = 1;

	public double getSalary() {
		return monthlyWage * monthsWorked;
	}

}
