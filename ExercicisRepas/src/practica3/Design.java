package practica3;

public class Design extends Employee {

	public Design(int employeeId, String name, int age) {
		super(employeeId, name, age);
	}

	int weeksWorked = 4;
	int weeklyWage = 499;

	public double getSalary() {
		return weeklyWage * weeksWorked;
	}
}
