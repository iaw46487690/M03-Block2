package practica3;

public class Employee {

	public int employeeId;
	public String name;
	public int age;

	public Employee(int employeeId, String name, int age) {
		this.employeeId = employeeId;
		this.name = name;
		this.age = age;
	}

	public String getData() {
		return "Id: " + employeeId + "\nNom: " + name + "\nEdat: " + age;
	}

	public void setData(int employeeId, String name, int age) {
		this.employeeId = employeeId;
		this.name = name;
		this.age = age;
	}

}
