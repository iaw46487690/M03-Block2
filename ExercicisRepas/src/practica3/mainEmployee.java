package practica3;

public class mainEmployee {

	public static void main(String[] args) {

		Tech tech1 = new Tech(1, "Alejandro", 20);
		Design design1 = new Design(2, "Raul", 23);
		HR hr1 = new HR(3, "Marta", 21);

		System.out.println("Tech");
		System.out.println("----");
		System.out.println(tech1.getData());
		System.out.println("Salari: " + tech1.getSalary());
		tech1.setData(4, "Fernando", 20);
		System.out.println(tech1.getData());
		System.out.println();
		System.out.println("Design");
		System.out.println("------");
		System.out.println(design1.getData());
		System.out.println("Salari: " + design1.getSalary());
		design1.setData(5, "Marcos", 23);
		System.out.println(design1.getData());
		System.out.println();
		System.out.println("HR");
		System.out.println("--");
		System.out.println(hr1.getData());
		System.out.println("Salari: " + hr1.getSalary());
		hr1.setData(6, "Sara", 21);
		System.out.println(hr1.getData());
	}

}
