package practica3;

public class Tech extends Employee {

	public Tech(int employeeId, String name, int age) {
		super(employeeId, name, age);
	}

	int hoursWorked = 40;
	int hourlyWage = 35;

	public double getSalary() {
		return hourlyWage * hoursWorked;
	}
}
