package practica2;

public class mainCircle {

	public static void main(String[] args) {
		Circle circle1 = new Circle(8);
		System.out.println("Circle 1");
		System.out.println("--------");
		System.out.println("Area: " + circle1.area(circle1.getRadius()));
		System.out.println("Perimetro: " + circle1.perimeter(circle1.getRadius()));
		System.out.println();
		Circle circle2 = new Circle();
		System.out.println("Circle 2");
		System.out.println("--------");
		System.out.println("Area: " + circle2.area(3));
		System.out.println("Perimetro: " + circle2.perimeter(3));
	}

}
