package practica2;

public class Circle {

	public double radius;

	public Circle() {
		this.radius = 5;
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public double area(double radius) {
		return Math.PI * Math.pow(radius, 2);
	}

	public double perimeter(double radius) {
		return 2 * Math.PI * radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

}
