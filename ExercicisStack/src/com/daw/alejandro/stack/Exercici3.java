package com.daw.alejandro.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici3 {

	public static void main(String[] args) {
		String[] expressio = {"25","+","3","*","(","1","+","2","+","(","30","*","4",")",")","/","2"};
		int i = 0;
		Deque<String> cua = new ArrayDeque<String>();
		for (String element : expressio) {
			if (element.equals("(") || element.equals(")")) {
				cua.add(element);
			}
		}
		System.out.println(cua);
		
		for (String element : cua) {
			if (!cua.isEmpty() && cua.peekFirst().equals("(") && element.equals(")")) { 
                i++;
				cua.pop();
           }
           else cua.push(element);
		}
		System.out.println(cua);
		if (!cua.isEmpty()) {
			System.out.println("Expressió matemàtica no ben formada"); 
		}
        else {
        	System.out.println("Expressions matem:" + i);
        }
	}

}
