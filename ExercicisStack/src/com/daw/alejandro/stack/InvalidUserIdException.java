package com.daw.alejandro.stack;

import java.util.Scanner;

public class InvalidUserIdException extends Exception {
	
	@Override
	public String toString() {
		return ("Invalid user id entered");
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int user_id=sc.nextInt();
		try {
			if (user_id!=1234) {
				throw new InvalidUserIdException();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
// paramtre b el cual vols dividir el primer pel segon