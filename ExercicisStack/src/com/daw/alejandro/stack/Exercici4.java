package com.daw.alejandro.stack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class Exercici4 {

	public static void main(String[] args) {
		Deque<String> cua1 = new LinkedList<String>();
        Deque<String> cua2 = new ArrayDeque<>();
        
        Deque<String> cua1Inversa = new LinkedList<String>();
        Deque<String> cua2Inversa = new ArrayDeque<String>();

        cua1.addAll(Arrays.asList("1","2","3","4"));
        cua2.addAll(Arrays.asList("1","2","3","4"));
        
        System.out.println("Invertir amb Iterator");
        Iterator<String> element = cua1.iterator();
		while (element.hasNext()) {
			cua1Inversa.push(element.next());
		}
		System.out.println(cua1Inversa);
		System.out.println();
		
		System.out.println("Invertir sense Iterator");
		for (String element2 : cua2) {
			cua2Inversa.push(element2);
		}
		System.out.println(cua2Inversa);
	}

}
