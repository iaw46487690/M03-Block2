package com.daw.alejandro.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici1 {

	public static <T> void main(String[] args) {
		Deque<Integer> stackDeque = new ArrayDeque<Integer>();
		Deque<T> invers = new ArrayDeque<T>();
		stackDeque.add(1);
		stackDeque.add(2);
		stackDeque.add(3);
		stackDeque.add(4);
		stackDeque.add(5);
		stackDeque.add(6);
		stackDeque.add(7);
		stackDeque.add(8);
		
		invertirPila(stackDeque, invers);
	}
	public static <T> void invertirPila(Deque<Integer> stackDeque, Deque<T> invers) {
		System.out.println("Pila: " + stackDeque);
		
		for (Integer element : stackDeque) {
			invers.push((T) element);
		}
		System.out.println("Pila Inversa: " + invers);
	}
}
