package com.daw.alejandro.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici2 {

	public static void main(String[] args) {
		String[] diamants = {"<","(","<",">",")",">"};
		Deque<String> pilaDiamants = new ArrayDeque<String>();
		int blancs = 0;
		int negres = 0;
		for (String element : diamants) {
			pilaDiamants.add(element);
		}
		System.out.println(pilaDiamants);
		for (String element : pilaDiamants) {
			if (pilaDiamants.peekFirst().equals("<") && element.equals(">")) {
				blancs++;
				pilaDiamants.pop();
			}
			if (pilaDiamants.peekFirst().equals("(") && element.equals(")")) {
				negres++;
				pilaDiamants.pop();
			}
		}
		//System.out.println(pilaDiamants);
		System.out.println("Hi han " + blancs + " blancs i " + negres + " negres");
	}
}
