package com.daw.alejandro.ecom;


import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author eva
 */
public class GestionPedido {

    public static void main(String[] args) throws SQLException {

    	//TODO Controlar excepcions aquí al main ...
    	
        PedidoJDBCDAO.loadDriver();
        PedidoJDBCDAO.connect();

        boolean salir = false;
        do {
            salir = menuPrincipal();
        } while (!salir);

        PedidoJDBCDAO.close();

    }

    public static boolean menuPrincipal() throws SQLException {
        System.out.println("");
        System.out.println("MENU PRINCIPAL");
        System.out.println("1. Listar pedidos");
        System.out.println("2. Nuevo pedidos");
        System.out.println("3. Modificar pedidos");
        System.out.println("4. Eliminar pedidos");
        System.out.println("5. Salir");
        
      //TODO ampliem menu amb Productes i comandes.....
        // Veure comandes ha de mostrar idProducte, idComanda i noms, quantitat
        
        Scanner in = new Scanner(System.in);
            
        int opcion = pideInt("Elige una opción: ");
        
        switch (opcion) {
            case 1:
                opcionMostrarPedidos();
                return false;
            case 2:
                opcionNuevoPedidos();
                return false;
            case 3:
                opcionModificarPedidos();
                return false;
            case 4:
                opcionEliminarPedidos();
                return false;
            case 5:
                return true;
            default:
                System.out.println("Opción elegida incorrecta");
                return false;
        }
        
    }
    
    public static int pideInt(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                int valor = in.nextInt();
                //in.nextLine();
                return valor;
            } catch (Exception e) {
                System.out.println("No has introducido un número entero. Vuelve a intentarlo.");
            }
        }
    }
    
    public static String pideLinea(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                String linea = in.nextLine();
                return linea;
            } catch (Exception e) {
                System.out.println("No has introducido una cadena de texto. Vuelve a intentarlo.");
            }
        }
    }

    public static void opcionMostrarPedidos() throws SQLException {
        System.out.println("Listado de Pedidos:");
        PedidoJDBCDAO.printTablaPedidos();
    }

    public static void opcionNuevoPedidos() throws SQLException {
        Scanner in = new Scanner(System.in);

        System.out.println("Introduce los datos del nuevo pedido:");
        int id = pideInt("Id: ");
        String nombre = pideLinea("Nombre: ");

        boolean res = PedidoJDBCDAO.insertPedidos(id, nombre);

        if (res) {
            System.out.println("Pedido registrado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionModificarPedidos() throws SQLException {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del pedido a modificar: ");

        // Comprobamos si existe el Pedidos
        if (!PedidoJDBCDAO.existsPedidos(id)) {
            System.out.println("El pedido " + id + " no existe.");
            return;
        }

        // Mostramos datos del Pedidos a modificar
        PedidoJDBCDAO.printPedidos(id);

        // Solicitamos los nuevos datos
        String nombre = pideLinea("Nuevo nombre: ");

        // Registramos los cambios
        boolean res = PedidoJDBCDAO.updatePedidos(id, nombre);

        if (res) {
            System.out.println("Pedido modificado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionEliminarPedidos() throws SQLException {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del pedido a eliminar: ");

        // Comprobamos si existe el Pedidos
        if (!PedidoJDBCDAO.existsPedidos(id)) {
            System.out.println("El pedido " + id + " no existe.");
            return;
        }

        // Eliminamos el Pedidos
        boolean res = PedidoJDBCDAO.deletePedidos(id);

        if (res) {
            System.out.println("Pedido eliminado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }
}
