package com.daw.alejandro.ecom;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class ClientJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "admin";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "clientes";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DIR = "direccion";

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     * @throws SQLException 
     */
    public static boolean connect() {
    	try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println(DB_MSQ_CONN_OK);
        	return true;
        } catch (Exception e) {
        	System.out.println(DB_MSQ_CONN_NO);
        	System.out.println(e);
        	return false;
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     * @throws SQLException 
     */
    public static boolean isConnected() throws SQLException {
        // Comprobamos estado de la conexión
    	if (conn.isValid(10) && !conn.isClosed()) {         		
    		return true;         	
    	}
        return false;
    }

    /**
     * Cierra la conexión con la base de datos
     * @throws SQLException 
     */
    public static void close() throws SQLException {
    	conn.close();
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA CLIENTES
    //////////////////////////////////////////////////
    ;
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla clientes de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     * @throws SQLException 
     */
    public static ResultSet getTablaClientes(int resultSetType, int resultSetConcurrency) throws SQLException {
    	Statement statment = conn.createStatement(resultSetType, resultSetConcurrency);
    	String sql = "select * from clientes";
    	ResultSet resultSet = statment.executeQuery(sql);
    	return resultSet;
    }

    /**
     * Obtiene toda la tabla clientes de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     * @throws SQLException 
     */
    public static ResultSet getTablaClientes() throws SQLException {
    	Statement statment = conn.createStatement();
    	String sql = "select * from clientes";
    	ResultSet resultSet = statment.executeQuery(sql);
    	return resultSet;
    }

    /**
     * Imprime por pantalla el contenido de la tabla clientes
     * @throws SQLException 
     */
    public static void printTablaClientes() throws SQLException {
    	ResultSet resultSet = getTablaClientes();
    	while (resultSet.next()) {
    		int id = resultSet.getInt(DB_CLI_ID);
    		String nom = resultSet.getString(DB_CLI_NOM);
    		String dir = resultSet.getString(DB_CLI_DIR);
    		System.out.println("Id: " + id + "\nNom: " + nom + "\nDireccio: " + dir);
    		System.out.println();
    	}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO CLIENTE
    //////////////////////////////////////////////////
    ;
    
    /**
     * Solicita a la BD el cliente con id indicado
     * @param id id del cliente
     * @return ResultSet con el resultado de la consulta, null en caso de error
     * @throws SQLException 
     */
    public static ResultSet getCliente(int id) throws SQLException {
    	PreparedStatement preparedStatement;
    	String sql = "select * from clientes where id = ?";
    	preparedStatement = conn.prepareStatement(sql);
    	preparedStatement.setInt(1, id);
    	ResultSet resultSet = preparedStatement.executeQuery();
    	return resultSet;
    }

    /**
     * Comprueba si en la BD existe el cliente con id indicado
     *
     * @param id id del cliente
     * @return verdadero si existe, false en caso contrario
     * @throws SQLException 
     */
    public static boolean existsCliente(int id) throws SQLException {
    	ResultSet resultSet = getCliente(id);
        if (resultSet == null) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Imprime los datos del cliente con id indicado  --> Carga objecto Client
     *
     * @param id id del cliente
     * @throws SQLException 
     */
    public static void printCliente(int id) throws SQLException {
    	if (existsCliente(id)) {
    		ResultSet resultSet = getCliente(id);
        	while (resultSet.next()) {
        		int idClient = resultSet.getInt(DB_CLI_ID);
        		String nom = resultSet.getString(DB_CLI_NOM);
        		String dir = resultSet.getString(DB_CLI_DIR);
        		System.out.println("Id: " + idClient + "\nNom: " + nom + "\nDireccio: " + dir);
        		System.out.println();
        	}
    	} else {
    		System.out.println("El client no existeix");
    	}
    }

    /**
     * Solicita a la BD insertar un nuevo registro cliente
     *
     * @param nombre nombre del cliente
     * @param direccion dirección del cliente
     * @return verdadero si pudo insertarlo, false en caso contrario
     * @throws SQLException 
     */
    public static boolean insertCliente(String nombre, String direccion) throws SQLException {
    	PreparedStatement preparedStatement;
    	String sql = "insert into clientes (nombre, direccion) VALUES (?, ?)";
    	preparedStatement = conn.prepareStatement(sql);
    	preparedStatement.setString(1, nombre);
    	preparedStatement.setString(2, direccion);
    	preparedStatement.execute();
    	return true;
    }

    /**
     * Solicita a la BD modificar los datos de un cliente
     *
     * @param id id del cliente a modificar
     * @param nombre nuevo nombre del cliente
     * @param direccion nueva dirección del cliente
     * @return verdadero si pudo modificarlo, false en caso contrario
     * @throws SQLException 
     */
    public static boolean updateCliente(int id, String nouNom, String novaDir) throws SQLException {
    	PreparedStatement preparedStatement;
    	String sql = "update clientes set nombre = ?, direccion = ? where id = ?";
    	preparedStatement = conn.prepareStatement(sql);
    	preparedStatement.setString(1, nouNom);
    	preparedStatement.setString(2, novaDir);
    	preparedStatement.setInt(3, id);
    	preparedStatement.execute();
    	return true;
    }

    /**
     * Solicita a la BD eliminar un cliente
     *
     * @param id id del cliente a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     * @throws SQLException 
     */
    public static boolean deleteCliente(int id) throws SQLException {
    	PreparedStatement preparedStatement;
    	String sql = "delete from clientes where id = ?";
    	preparedStatement = conn.prepareStatement(sql);
    	preparedStatement.setInt(1, id);
    	preparedStatement.execute();
    	return true;
    }

}
