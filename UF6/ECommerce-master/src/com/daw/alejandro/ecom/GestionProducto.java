package com.daw.alejandro.ecom;


import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author eva
 */
public class GestionProducto {

    public static void main(String[] args) throws SQLException {

    	//TODO Controlar excepcions aquí al main ...
    	
        ProductoJDBCDAO.loadDriver();
        ProductoJDBCDAO.connect();

        boolean salir = false;
        do {
            salir = menuPrincipal();
        } while (!salir);

        ProductoJDBCDAO.close();

    }

    public static boolean menuPrincipal() throws SQLException {
        System.out.println("");
        System.out.println("MENU PRINCIPAL");
        System.out.println("1. Listar productos");
        System.out.println("2. Nuevo producto");
        System.out.println("3. Modificar producto");
        System.out.println("4. Eliminar producto");
        System.out.println("5. Salir");
        
      //TODO ampliem menu amb Productes i comandes.....
        // Veure comandes ha de mostrar idProducte, idComanda i noms, quantitat
        
        Scanner in = new Scanner(System.in);
            
        int opcion = pideInt("Elige una opción: ");
        
        switch (opcion) {
            case 1:
                opcionMostrarProductos();
                return false;
            case 2:
                opcionNuevoProducto();
                return false;
            case 3:
                opcionModificarProducto();
                return false;
            case 4:
                opcionEliminarProducto();
                return false;
            case 5:
                return true;
            default:
                System.out.println("Opción elegida incorrecta");
                return false;
        }
        
    }
    
    public static int pideInt(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                int valor = in.nextInt();
                //in.nextLine();
                return valor;
            } catch (Exception e) {
                System.out.println("No has introducido un número entero. Vuelve a intentarlo.");
            }
        }
    }
    
    public static String pideLinea(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                String linea = in.nextLine();
                return linea;
            } catch (Exception e) {
                System.out.println("No has introducido una cadena de texto. Vuelve a intentarlo.");
            }
        }
    }
    
    public static double pideDouble(String mensaje){
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                double valor = in.nextDouble();
                return valor;
            } catch (Exception e) {
                System.out.println("No has introducido un double. Vuelve a intentarlo.");
            }
        }
    }

    public static void opcionMostrarProductos() throws SQLException {
        System.out.println("Listado de Productos:");
        ProductoJDBCDAO.printTablaProductos();
    }

    public static void opcionNuevoProducto() throws SQLException {
        Scanner in = new Scanner(System.in);

        System.out.println("Introduce los datos del nuevo producto:");
        int idProducto = pideInt("Nuevo id: ");
        String nombre = pideLinea("Nuevo nombre: ");
        double precio = pideDouble("Nuevo precio: ");

        boolean res = ProductoJDBCDAO.insertProductos(idProducto, nombre, precio);
        
        if (res) {
            System.out.println("Producto registrado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionModificarProducto() throws SQLException {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del producto a modificar: ");

        // Comprobamos si existe el producto
        if (!ProductoJDBCDAO.existsProductos(id)) {
            System.out.println("El producto " + id + " no existe.");
            return;
        }

        // Mostramos datos del producto a modificar
        ProductoJDBCDAO.printProductos(id);

        // Solicitamos los nuevos datos
        int idProducto = pideInt("Nuevo id: ");
        String nombre = pideLinea("Nuevo nombre: ");
        double precio = pideDouble("Nuevo precio: ");

        // Registramos los cambios
        boolean res = ProductoJDBCDAO.updateProductos(idProducto, nombre, precio);

        if (res) {
            System.out.println("Producto modificado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionEliminarProducto() throws SQLException {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del producto a eliminar: ");

        // Comprobamos si existe el producto
        if (!ProductoJDBCDAO.existsProductos(id)) {
            System.out.println("El producto " + id + " no existe.");
            return;
        }

        // Eliminamos el producto
        boolean res = ProductoJDBCDAO.deleteProductos(id);

        if (res) {
            System.out.println("Producto eliminado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }
}
