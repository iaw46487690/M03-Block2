package Varies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import Llibreries.Cadena;

public abstract class Data {
	
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	public static String imprimirData(LocalDateTime dataTmp) {
        if (dataTmp.equals(null)) {
            return "NULL";
        } /*else {
            return LocalDateTime.format(formatter);
        }*/
		return null;//va fora
    }
	
	public static boolean esData(String dataTmp) {
		boolean esData = true; 
		String[] data;
		data = dataTmp.split("-");
		if (data.length != 3) {
			esData = false;
		} else {
			String dia = data[0];
			String mes = data[1];
			String any = data[2];
			boolean numDia = Cadena.stringIsInt(dia);
			boolean numMes = Cadena.stringIsInt(mes);
			boolean numAny = Cadena.stringIsInt(any);
			if (numDia == false || numMes == false || numAny == false
					|| dia.length() != 2 || mes.length() != 2 || any.length() != 4) {
				esData = false;
			}
		}
		return esData;
	}
}
