package com.daw.alejandrocarreño.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Ruta_Dades {
	private int id;                             	
	private String nom;
	private ArrayList<Integer> waypoints;       	
	private boolean actiu;                      	
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;             
	private LocalDateTime dataModificacio;
	
	/**
	 * @param id
	 * @param nom
	 * @param waypoints
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        return "\tid = " + id + ":" + 
                "\n\tnom = " + nom +
                "\n\twaypoints = " + waypoints; /*+
                "\nactiu = " + actiu +
                "\ndataCreacio = " + dataCreacio.format(formatter) +
                "\ndataAnulacio = " + dataAnulacio.format(formatter) +
                "\ndataModificacio = " + dataModificacio.format(formatter);*/
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(ArrayList<Integer> waypoints) {
		this.waypoints = waypoints;
	}
}
