package com.daw.alejandrocarreño.krona;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import Llibreries.Cadena;

import java.time.LocalDateTime;
import Varies.Data;

public class Ruta {
	
	public static List<Waypoint_Dades>crearRutaInicial() {
		List<Waypoint_Dades> comprovacioRendimentTmp = new ArrayList<Waypoint_Dades>();
		return comprovacioRendimentTmp;
	}
	
	// Menu 10 de Krona
	public static ComprovacioRendiment inicialitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		crearRutaInicial();
		for (int i = 0; i < 8; i++) {
			comprovacioRendimentTmp.pilaWaypoints.push(new Waypoint_Dades(i, "Òrbita de Júpiter REPETIDA", new int[] {i,i,i}, true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		}
		Waypoint_Dades waypoint = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] {4,4,4}, true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		comprovacioRendimentTmp.pilaWaypoints.push(waypoint);
		comprovacioRendimentTmp.wtmp = waypoint;
		
		return comprovacioRendimentTmp;
	}
	
	// Menu 11 de Krona
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Iterator<Waypoint_Dades> element = comprovacioRendimentTmp.pilaWaypoints.iterator();
		while (element.hasNext()) {
			System.out.println(element.next().toString());
		}
	}
	
	// Menu 12 de Krona
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
		for (Waypoint_Dades element : comprovacioRendimentTmp.pilaWaypoints) {
			pilaWaypointsInversa.push(element);
		}
		for (Waypoint_Dades element2 : pilaWaypointsInversa) {
			System.out.println(element2.toString());
		}
	}
	
	// Menu 13 de Krona
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		boolean found = false;
		for (Waypoint_Dades element : comprovacioRendimentTmp.pilaWaypoints) {
			if (element.equals(comprovacioRendimentTmp.wtmp)) {
				System.out.println("SI hem trobat el waypoint Òrbita de Júpiter REPETIDA emmagatzemat en comprovacioRendimentTmp.wtmp, en la llista");
				break;
			}
		}
		Waypoint_Dades newWaypoint = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] {4,4,4}, true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		for (Waypoint_Dades element : comprovacioRendimentTmp.pilaWaypoints) {
			if (element.equals(newWaypoint)) {
				System.out.println("SI hem trobat el waypoint Òrbita de Júpiter REPETIDA emmagatzemat en comprovacioRendimentTmp.wtmp, en la llista");
				found = true;
				break;
			}
		}
		if (found == false) {
			System.out.println("NO hem trobat el waypoint Òrbita de Júpiter creat ara mateix, en la llista.");
		}
	}
	
	// Menu 20 de Krona
    public static ComprovacioRendiment inicialitzaLListaRutes(ComprovacioRendiment comprovacioRendimentTmp) {
    	Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta 0: Terra --> Punt Lagrange Júpiter-Europa", new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true, LocalDateTime.parse("28-10-2020 16:30", Data.formatter), null, LocalDateTime.parse("28-10-2020 16:30", Data.formatter));
    	Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> Òrbita de Mart (directe)", new ArrayList<Integer>(Arrays.asList(0, 3)), true, LocalDateTime.parse("28-10-2020 16:31", Data.formatter), null, LocalDateTime.parse("28-10-2020 16:31", Data.formatter));
    	Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> Òrbita de Venus", new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true, LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null, LocalDateTime.parse("28-10-2020 16:32", Data.formatter));
    	Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> Òrbita de Júpiter ", new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true, LocalDateTime.parse("28-10-2020 16:33", Data.formatter), null, LocalDateTime.parse("28-10-2020 16:33", Data.formatter));
    	Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta 2.2: Terra --> Òrbita de Venus (REPETIDA)", new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true, LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null, LocalDateTime.parse("30-10-2020 19:49",Data.formatter));
        comprovacioRendimentTmp.llistaRutes.add(ruta_0);
        comprovacioRendimentTmp.llistaRutes.add(ruta_1);
        comprovacioRendimentTmp.llistaRutes.add(ruta_2);
        comprovacioRendimentTmp.llistaRutes.add(ruta_3);
        comprovacioRendimentTmp.llistaRutes.add(ruta_4);
        for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
        	System.out.println(element.getNom() + ": waypoints" + element.getWaypoints());
        }
		return comprovacioRendimentTmp;
    }
    
    // Menu 21 de Krona
    public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
    	Set<Integer> unio = new HashSet<Integer>();
    	for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
    		if (!unio.contains(element.getWaypoints())) {
    			unio.addAll(element.getWaypoints());
    		}
    	}
    	System.out.println("ID dels waypoints ficats en el set: " + unio);
    }
    
    // Menu 22 de Krona
    public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
    	Set<Integer> interseccio = new HashSet<Integer>();
    	Set<Integer> remove = new HashSet<Integer>();
    	for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
    		if (!interseccio.contains(element.getWaypoints())) {
    			interseccio.addAll(element.getWaypoints());
    		}
    	}
    	for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
    		remove.addAll(interseccio);
    		if (!element.getWaypoints().contains(interseccio)) {
    			remove.removeAll(element.getWaypoints());
    			interseccio.removeAll(remove);
    		}
    	}
    	System.out.println("ID dels waypoints en totes les rutes: " + interseccio);
    }
    
    private static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
    	int posicio = -1;
        Set<Integer> ids = new HashSet<Integer>();
        for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
        	ids.add(element.getId());
        }
    	if (ids.contains(numRuta)) {
    		posicio = comprovacioRendimentTmp.llistaRutes.indexOf(comprovacioRendimentTmp.llistaRutes.get(numRuta));
    	}
    	return posicio;
    }
    
    // Menu 23 de Krona
    public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
    	Scanner sc = new Scanner(System.in);
    	Set<Integer> ids = new HashSet<Integer>();
    	String idsRuta = "";
		String[] rutes;
		String rutaA = null, rutaB = null;
		boolean r1 = false, r2 = false;
		int rutaAInt = 0, rutaBInt = 0 , brA = 0, brB = 0;
    	for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
        	System.out.println("ID " + element.getId() + ": " + element.getNom() + ": waypoints" + element.getWaypoints());
    	}
    	System.out.println();
    	while (r1 == false || r2 == false || idsRuta.split(",").length > 2 
    			|| brA == -1 || brB == -1) {
	    	System.out.print("Selecciona ruta A i B (format: 3,17): ");
	    	idsRuta = sc.next();
	    	rutes = idsRuta.split(",");
	    	rutaA = rutes[0];
	    	rutaB = rutes[1];
	    	r1 = Cadena.stringIsInt(rutaA);
			r2 = Cadena.stringIsInt(rutaB);
			if (rutes.length != 2) {
				System.out.println("ERROR: introduir 2 paràmetres separats per 1 coma. Has introduit " + rutes.length + " paràmetres.");
			} else {
				if (r1 == false) {
					System.out.println("ERROR: has introduit " + rutaA + " com a ruta. Els ID de les rutes són integers.");
				}
				if (r2 == false) {
					System.out.println("ERROR: has introduit " + rutaB + " com a ruta. Els ID de les rutes són integers.");
				}
			}
			if (r1 == true && r2 == true && idsRuta.split(",").length == 2) {
				rutaAInt = Integer.parseInt(rutaA);
				rutaBInt = Integer.parseInt(rutaB);
				brA = buscarRuta(rutaAInt, comprovacioRendimentTmp);
				brB = buscarRuta(rutaBInt, comprovacioRendimentTmp);
			}
			if (r1 == true && r2 == true && idsRuta.split(",").length == 2 
					&& brA != -1 && brB != -1) {
				System.out.println();
				ids.addAll(comprovacioRendimentTmp.llistaRutes.get(rutaAInt).getWaypoints());
				System.out.println("HashSet (havent-hi afegit els waypoints de la ruta A) = " + ids);
				ids.removeAll(comprovacioRendimentTmp.llistaRutes.get(rutaBInt).getWaypoints());
				System.out.println("HashSet (havent-hi tret els waypoints de la rutaB) = " + ids);
			} else if (r1 == true && r2 == true && idsRuta.split(",").length == 2) {
				if (brA == -1) {
					System.out.println("ERROR: no existeix la ruta " + rutaAInt + " en el sistema");
				}
				if (brB == -1) {
					System.out.println("ERROR: no existeix la ruta " + rutaBInt + " en el sistema");
				}
			}
    	}
    }
    
    // Menu 24 de Krona
    public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
    	// No funciona TreeSet
    	//TreeSet<Ruta_Dades> ts = new TreeSet<Ruta_Dades>();
    	Set<Ruta_Dades> ts = new HashSet<Ruta_Dades>();
    	boolean repetit = false;
    	for (Ruta_Dades element : comprovacioRendimentTmp.llistaRutes) {
    		for (Ruta_Dades element2 : ts) {
    			if (element.getWaypoints().equals(element2.getWaypoints())) {
    				repetit = true;
        		}
    		}
    		if (repetit == false) {
    			System.out.println("ID " + element.getId() + ": " + element.getNom() + ": waypoints" + element.getWaypoints());
    			ts.add(element);
    		}
    		repetit = false;		
    	}
    }
    
    // Menu 30 de Krona
    public static void crearLinkedHashMapDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
        int key;
    	Map<Integer, Ruta_Dades> mapa = new HashMap<Integer, Ruta_Dades>();
        
        for (int i = 0; i < comprovacioRendimentTmp.llistaRutes.size(); i++) {
        	mapa.put(i+1, comprovacioRendimentTmp.llistaRutes.get(i));
        }
        
        for (int i = 0; i < mapa.size(); i++) {
			comprovacioRendimentTmp.mapaLinkedDeRutes.put(i, mapa.get(i));
		}
        
        long tempsInici1 = System.nanoTime();
        Set set = mapa.entrySet();
        Iterator it1 = set.iterator();
        System.out.println("1a forma de visualitzar el contingut del map (map->set):");
        while(it1.hasNext()) {
           Map.Entry me = (Map.Entry)it1.next();
           System.out.println("Clau del map = " + me.getKey() + ":\nDades de la ruta:\n" + me.getValue());
        }
        long tempsFinal1 = System.nanoTime();
        
        long tempsInici2 = System.nanoTime();
        System.out.println();
        System.out.println("2a forma de visualitzar el contingut del map (iterator de map):");
        Iterator<Integer> it2 = mapa.keySet().iterator();
        while (it2.hasNext()) {
            key = it2.next();
            System.out.println(key + ": " + mapa.get(key));
        }
        long tempsFinal2 = System.nanoTime();
        
        long tempsInici3 = System.nanoTime();
        System.out.println();
        System.out.println("3a forma de visualitzar el contingut del map (for-each):");
        for (Entry<Integer, Ruta_Dades> dada : mapa.entrySet()) {
            System.out.println(dada.getKey() + ": " + dada.getValue().toString());
        }
        long tempsFinal3 = System.nanoTime();
        
        System.out.println();
        System.out.println("TEMPS PER 1a FORMA (map --> set + iterador del set): " + (tempsFinal1 - tempsInici1));
        System.out.println("TEMPS PER 1a FORMA (iterador de les claus del map): " + (tempsFinal2 - tempsInici2));
        System.out.println("TEMPS PER 1a FORMA (for-each del map --> set): " + (tempsFinal3 - tempsInici3));
    }
    
    // Menu 31 de Krona
    public static void visualitzarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
    	String num = "";
    	boolean n1 = false;
    	Scanner sc = new Scanner(System.in);
    	while (n1 == false) {
	    	System.out.print("Escriu el nº del waypoint que vols buscar: ");
	    	num = sc.next();
	    	n1 = Cadena.stringIsInt(num);
	    	if (n1 == false) {
	    		System.out.println("ERROR: has introduit " + num + " com a ruta. Els ID de les rutes són integers.");
	    	}
    	}
    	System.out.println("RUTES QUE CONTENEN EL WAYPOINT " + num + ":");
    	int numInt = Integer.parseInt(num);
        for (Entry<Integer, Ruta_Dades> element : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
        	if (element.getValue().getWaypoints().contains(numInt)) {
        		System.out.println(element.getValue());
        	}
        }
    }
    
    
    
}
