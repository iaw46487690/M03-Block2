package com.daw.alejandrocarreño.krona;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import Llibreries.Cadena;
import Varies.Data;
import Varies.Tipus_Waypoint;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Waypoint {
	
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		return comprovacioRendimentTmp;
	}
	
	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear, ComprovacioRendiment comprovacioRendimentTmp) {
		long tempsInicial;
		long tempsFinal;
		long tempsEnNanosegons;
		long tempsEnMilisegons;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int[] coordenadesTmp = new int [] {0,0,0};
		
		//Waypoint_Dades waypoint = new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
		// ArrayList
		tempsInicial = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		}
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		tempsEnMilisegons = tempsEnNanosegons / 100000;
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en l'ArrayList: " + tempsEnMilisegons);
		
		// LinkedList
		tempsInicial = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		}
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		tempsEnMilisegons = tempsEnNanosegons / 100000;
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en el LinkedList: " + tempsEnMilisegons);
		
		return comprovacioRendimentTmp;
	}
	
	public static  ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		long tempsInicial;
		long tempsFinal;
		long tempsEnNanosegons;
		int meitatLlista = 10 / 2;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int[] coordenadesTmp = new int [] {0,0,0};
		
		Waypoint_Dades waypoint = new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
		
		System.out.println("llistaArrayList.size(): " + comprovacioRendimentTmp.llistaArrayList.size() + ", meitatLlista: " + meitatLlista);
		// Principi de l'array
		tempsInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(1, waypoint);
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		System.out.println("Temps per a insertar 1 waypoint en la 1a posició de l'ArrayList: " + tempsEnNanosegons);
		
		tempsInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(1, waypoint);
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		System.out.println("Temps per a insertar 1 waypoint en la 1a posició del LinkedList: " + tempsEnNanosegons);
		System.out.println("----------------");
		
		// En mig de l'array
		tempsInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(meitatLlista, waypoint);
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		System.out.println("Temps per a insertar 1 waypoint al mig (pos. 25000) de l'ArrayList: " + tempsEnNanosegons);
		
		tempsInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(meitatLlista, waypoint);
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		System.out.println("Temps per a insertar 1 waypoint al mig (pos. 25000) del LinkedList: " + tempsEnNanosegons);
		System.out.println("----------------");
		
		// Final de l'array
		tempsInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(waypoint);
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		System.out.println("Temps per a insertar 1 waypoint al final de l'ArrayList: " + tempsEnNanosegons);
		
		tempsInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(waypoint);
		tempsFinal = System.nanoTime();
		tempsEnNanosegons = tempsFinal - tempsInicial;
		System.out.println("Temps per a insertar 1 waypoint al final del LinkedList: " + tempsEnNanosegons);
		
		return comprovacioRendimentTmp;
	}
	
	public static  ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		List<Integer> idsPerArrayList = new ArrayList<Integer>(comprovacioRendimentTmp.llistaArrayList.size());
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			idsPerArrayList.add(i, i);
		}
		System.out.println("---- APARTAT 1 ----");
		System.out.println("S'ha inicialitzat la llista idsPerArrayList amb " + idsPerArrayList.size() + " elements.");
		System.out.println("El 1r element té el valor: " + idsPerArrayList.get(0));
		System.out.println("L'ultim element té el valor: " + idsPerArrayList.get(idsPerArrayList.size() - 1));
		System.out.println();
		
		System.out.println("---- APARTAT 2 ----");
		for (Integer element : idsPerArrayList) {
			System.out.println("ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(" + element + ").getId(): " + 
					comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			
			comprovacioRendimentTmp.llistaArrayList.get(element).setId(idsPerArrayList.get(element));
			
			System.out.println("DESPRÉS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(" + element + ").getId(): " + 
					comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			System.out.println();
		}
		
		System.out.println("---- APARTAT 3.1 (bucle for) ----");
		for (Integer element : idsPerArrayList) {
			System.out.println("ID = " + comprovacioRendimentTmp.llistaArrayList.get(element).getId() + ", nom = " + 
					comprovacioRendimentTmp.llistaArrayList.get(element).getNom());
		}
		
		System.out.println();
		System.out.println("---- APARTAT 3.2 (Iterator) ----");
		ListIterator<Waypoint_Dades> element = comprovacioRendimentTmp.llistaArrayList.listIterator();
        while(element.hasNext()){
        	System.out.print("ID = " + element.next().getId());
        	element.previous();
			System.out.println(", nom = " + element.next().getNom());
        }
        
        System.out.println();
		System.out.println("---- APARTAT 4 ----");
		System.out.println("Preparat per esborrar el contingut de llistaLinkedList que té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.println("Esborrada. Ara llistaLinkedList té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			comprovacioRendimentTmp.llistaLinkedList.add(comprovacioRendimentTmp.llistaArrayList.get(i));
		}
		System.out.println("Copiats els elements de llistaArrayList en llistLinkedList que ara té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		
		System.out.println();
		System.out.println("---- APARTAT 5.1 (bucle for) ----");
		for (Integer element5 : idsPerArrayList) {
			if (comprovacioRendimentTmp.llistaArrayList.get(element5).getId() > 5) {
				comprovacioRendimentTmp.llistaArrayList.get(element5).setNom("Òrbita de Mart");
				System.out.println("Modificat el waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(element5).getId());
			}
		}
		System.out.println();
		System.out.println("---- APARTAT 5.1 (comprovació) ----");
		for (Integer element5 : idsPerArrayList) {
			System.out.println("El waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(element5).getId() + " té el nom = " + comprovacioRendimentTmp.llistaArrayList.get(element5).getNom());
		}
		
		System.out.println();
		System.out.println("---- APARTAT 5.2 (Iterator) ----");
		ListIterator<Waypoint_Dades> element5_2 = comprovacioRendimentTmp.llistaArrayList.listIterator();
        while(element5_2.hasNext()){
        	if (element5_2.next().getId() < 5) {
        		element5_2.previous();
        		element5_2.next().setNom("Punt Lagrange entre la Terra i la LLuna");
        		element5_2.previous();
        		System.out.println("Modificat el waypoint amb id = " + element5_2.next().getId());
        	}
        }
        
        System.out.println();
        System.out.println("---- APARTAT 5.2 (comprovació) ----");
        
        while(element5_2.hasPrevious()){
        	element5_2.previous();
        }
        while(element5_2.hasNext()){
        	System.out.print("El waypoint amb id = " + element5_2.next().getId());
        	element5_2.previous();
        	System.out.println(" té el nom = " + element5_2.next().getNom());
        }
        System.out.println();
		return comprovacioRendimentTmp;
	}
	
	public static  ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		int i = 0;
		for (Waypoint_Dades element : comprovacioRendimentTmp.llistaArrayList) {
			if (comprovacioRendimentTmp.llistaArrayList.get(i).getId() < 6) {
				comprovacioRendimentTmp.llistaArrayList.remove(element);
			}
			i++;
		}
		
		System.out.println("---- APARTAT 2 (Iterator) ----");
		ListIterator<Waypoint_Dades> element2 = comprovacioRendimentTmp.llistaLinkedList.listIterator();
        while(element2.hasNext()){
        	if (element2.next().getId() > 4) {
        		element2.previous();
        		System.out.println("Esborrat el waypoint amb id = " + element2.next().getId());
        		element2.remove();
        	}
        }
        
        System.out.println();
        System.out.println("---- APARTAT 2 (comprovació) ----");
        
        while(element2.hasPrevious()){
        	element2.previous();
        }
        while(element2.hasNext()){
        	System.out.print("El waypoint amb id = " + element2.next().getId());
        	element2.previous();
        	System.out.println(" té el nom = " + element2.next().getNom());
        }
		
        System.out.println();
        System.out.println("---- APARTAT 3 (listIterator) ----");
        ListIterator<Waypoint_Dades> element3 = comprovacioRendimentTmp.llistaLinkedList.listIterator();
        while(element3.hasNext()){
        	if (element3.next().getId() == 2) {
        		element3.previous();
        		System.out.println("Esborrat el waypoint amb id = " + element3.next().getId());
        		element3.remove();
        	}
        }
        
        System.out.println();
        System.out.println("---- APARTAT 3 (comprovació) ----");
        while(element3.hasPrevious()){
        	System.out.print("El waypoint amb id = " + element3.previous().getId());
        	element3.next();
        	System.out.println(" té el nom = " + element3.previous().getNom());
        }
		return comprovacioRendimentTmp;
	}
	
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String nouNom = "";
		String nouCoord = "";
		String[] nums;
		String n1 = null, n2 = null, n3 = null;
		boolean b1 = false, b2 = false, b3 = false;
		int novesCoordenades[] = new int[3];
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			if (comprovacioRendimentTmp.llistaArrayList.get(i).getId() % 2 == 0) {
				System.out.println("----- Modificar el waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(i).getId() + " -----");
				System.out.println("Nom actual: " + comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
				System.out.print("Nou nom: ");
				nouNom = sc.next();
				comprovacioRendimentTmp.llistaArrayList.get(i).setNom(nouNom);
				while (b1 == false || b2 == false || b3 == false || nouCoord.split(",").length > 3) {
					System.out.println("Coordenades actuals: " + comprovacioRendimentTmp.llistaArrayList.get(i).getCoordenadesString());
					System.out.print("Coordenades noves (format: 1,13,7): ");
					nouCoord = sc.next();
					nums = nouCoord.split(",");
					n1 = nums[0];
					n2 = nums[1];
					n3 = nums[2];
					b1 = Cadena.stringIsInt(n1);
					b2 = Cadena.stringIsInt(n2);
					b3 = Cadena.stringIsInt(n3);
					if (nouCoord.split(",").length > 3) {
						System.out.println("ERROR: introduir 3 paràmetres separats per 1 coma. Has introduit " + nouCoord.split(",").length + " paràmetres.");
					} else {
						if (b1 == false) {
						System.out.println("ERROR: coordenada " + n1 + " no vàlida.");
						}
						if (b2 == false) {
							System.out.println("ERROR: coordenada " + n2 + " no vàlida.");
						}
						if (b3 == false) {
							System.out.println("ERROR: coordenada " + n3 + " no vàlida.");
						}
					}
				}
				b1 = false;
				b2 = false;
				b3 = false;
				novesCoordenades[0] = Integer.parseInt(n1);
				novesCoordenades[1] = Integer.parseInt(n2);
				novesCoordenades[2] = Integer.parseInt(n3);
				comprovacioRendimentTmp.llistaArrayList.get(i).setCoordenades(novesCoordenades);
				System.out.println();
			}
		}
		return comprovacioRendimentTmp;
	}
	
	public static void visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		ListIterator<Waypoint_Dades> element = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		while (element.hasNext()) {
			System.out.println(element.next().toString());
		}
	}
	
	public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Distancia maxima de la Terra: ");
		int distanciaMaxima = sc.nextInt();
		int[] coords = null;
		int n1, n2, n3, distancia, i = 0;
		for (Waypoint_Dades element : comprovacioRendimentTmp.llistaArrayList) {
			coords = comprovacioRendimentTmp.llistaArrayList.get(i).getCoordenades();
			n1 = coords[0];
			n2 = coords[1];
			n3 = coords[2];
			distancia = (int) (Math.pow(n1, 2) + Math.pow(n2, 2) + Math.pow(n3, 2));
			if (distancia <= distanciaMaxima) {
				System.out.println(comprovacioRendimentTmp.llistaArrayList.get(i).toString());
			}
			i++;
		}
	}
	
	// Menu 40 de Krona
	public static void inicialitzarDadesWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		if (!comprovacioRendimentTmp.llistaWaypoints.isEmpty()) {
			comprovacioRendimentTmp.llistaWaypoints.clear();
		}
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] {0,0,0}, true, LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] {1,1,1}, true, LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),6));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] {2,2,2}, true, LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),1));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(3, "Òrbita de Mart", new int[] {3,3,3}, true, LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),0));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] {4,4,4}, true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),0));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa", new int[] {5,5,5}, true, LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),6));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(6, "Òrbita de Europa", new int[] {6,6,6}, true, LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),0));
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(7, "Òrbita de Venus", new int[] {7,7,7}, true, LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter),0));
	}
	
	// Menu 41 de Krona
	public static void nouWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		int id = 0, numTipus = -1;
		String coordenades = "", c1 = "", c2 = "", c3 = "", actiu = "", dataTmp = "", tipus = "";
		String[] numCoord;
		boolean numc1 = false, numc2 = false, numc3 = false, isActiu = false, bActiu = false;
		int novesCoordenades[] = new int[3];
		for (Waypoint_Dades element : comprovacioRendimentTmp.llistaWaypoints) {
			if (element.getId() > id) {
				id = element.getId();
			}
		}
		id++;
		System.out.print("Nom del Waypoint: ");
		String nomWaypoint = sc.next();
		while (coordenades.split(",").length != 3 || numc1 == false || numc2 == false || numc3 == false) {
			System.out.print("Coordenades (format: 1,13,7): ");
			coordenades = sc.next();
			numCoord = coordenades.split(",");
			c1 = numCoord[0];
			c2 = numCoord[1];
			c3 = numCoord[2];
			numc1 = Cadena.stringIsInt(c1);
			numc2 = Cadena.stringIsInt(c2);
			numc3 = Cadena.stringIsInt(c3);
			if (numCoord.length != 3) {
				System.out.println("ERROR: introduir 3 paràmetres separats per 1 coma. Has introduit " + numCoord.length + " paràmetres.");
			} else {
				if (numc1 == false) {
					System.out.println("ERROR: coordenada " + c1 + " no vàlida");	
				}
				if (numc2 == false) {
					System.out.println("ERROR: coordenada " + c2 + " no vàlida");	
				}
				if (numc3 == false) {
					System.out.println("ERROR: coordenada " + c3 + " no vàlida");	
				}
			}
		}
		novesCoordenades[0] = Integer.parseInt(c1);
		novesCoordenades[1] = Integer.parseInt(c2);
		novesCoordenades[2] = Integer.parseInt(c3);
		while (isActiu == false) {
			System.out.print("Actiu (format: true|false): ");
			actiu = sc.next();
			if (actiu.toLowerCase().equals("true") || actiu.toLowerCase().equals("false")) {
				isActiu = true;
				if (actiu.toLowerCase().equals("true")) {
					bActiu = true;
				} else {
					bActiu = false;
				}
			}
		}
		Date dataCreacio = new Date();
		while (Data.esData(dataTmp) == false) {
			System.out.print("Data d'anulació (DD-MM-AAAA): ");
			dataTmp = sc.next();
			if (Data.esData(dataTmp) == false) {
				System.out.println("ERROR: data de creació " + dataTmp + " no vàlida.");
			}
		}
		Date dataModificacio = new Date();
		for (int i = 0; i < Tipus_Waypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.println(i + ": " + Tipus_Waypoint.TIPUS_WAYPOINT[i]);
		}
		while (numTipus > Tipus_Waypoint.TIPUS_WAYPOINT.length-1 ||
				numTipus < 0) {
			System.out.print("Tipus de waypoint (format: 3): ");
			tipus = sc.next();
			boolean btipus = Cadena.stringIsInt(tipus);
			if (btipus == true) {
				numTipus = Integer.parseInt(tipus);
				if (numTipus > 6 || numTipus < 0) {
					System.out.println("ERROR: introduir 1 número dels visualitzats en la llista anterior. El número " + tipus + " no existeix.");
				}
			} else {
				System.out.println("ERROR: introduir 1 número dels visualitzats en la llista anterior. El número " + tipus + " no existeix.");
			}
		}
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(id, nomWaypoint, novesCoordenades, bActiu, LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null, LocalDateTime.parse("22-10-2020 23:55", Data.formatter), numTipus));
	}
	
	// Menu 42 de Krona
	public static void waypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		LinkedList<Waypoint_Dades> llistaA = new LinkedList<Waypoint_Dades>();
		LinkedList<Waypoint_Dades> llistaB = new LinkedList<Waypoint_Dades>();
		int numTipus = -1;
		String tipus = "";
		System.out.println("Tipus de waypoints disponibles: ");
		for (int i = 0; i < Tipus_Waypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.println(i + ": " + Tipus_Waypoint.TIPUS_WAYPOINT[i]);
		}
		while (numTipus > Tipus_Waypoint.TIPUS_WAYPOINT.length-1 ||
				numTipus < 0) {
			System.out.print("Tipus de waypoint (format: 3): ");
			tipus = sc.next();
			boolean btipus = Cadena.stringIsInt(tipus);
			if (btipus == true) {
				numTipus = Integer.parseInt(tipus);
				if (numTipus > 6 || numTipus < 0) {
					System.out.println("ERROR: introduir 1 número dels visualitzats en la llista anterior. El número " + tipus + " no existeix.");
				}
			} else {
				System.out.println("ERROR: introduir 1 número dels visualitzats en la llista anterior. El número " + tipus + " no existeix.");
			}
		}
		llistaA.addAll(comprovacioRendimentTmp.llistaWaypoints);
		for (Waypoint_Dades element : llistaA) {
			if (element.getTipus() == numTipus) {
				llistaB.add(element);
				System.out.println("Esborrat el waypoint amb id = " + element.getId() + " del tipus " + element.getTipus() + ". Waypoint llistaA --> llistaB");
				//llistaA.remove(element);
			}
		}
		System.out.println();
		System.out.println("LListaB de waypoints del tipus " + numTipus);
		for (Waypoint_Dades element : llistaB) {
			System.out.println("id " + element.getId() + ": " + element.getNom() + ", de tipus " + element.getTipus());
		}
	}
	
	// Menu 43 de Krona
	public static void numWaypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Waypoint_Dades> waypoints = new HashSet<Waypoint_Dades>();
		Map<Integer, Waypoint_Dades> mapa = new HashMap<Integer, Waypoint_Dades>();
		waypoints.addAll(comprovacioRendimentTmp.llistaWaypoints);
	}
	
	// Menu 44 de Krona
	public static void trobarWaypointsVsNom(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nom que voleu buscar: ");
		String nom = sc.next();
		for (Waypoint_Dades element : comprovacioRendimentTmp.llistaWaypoints) {
			if (element.getNom().toLowerCase().contains(nom.toLowerCase())) {
				System.out.println("id " + element.getId() + ": " + element.getNom());
			}
		}
	}
	
	// Menu 45 de Krona
	public static void ordenarWaypointsPerData(ComprovacioRendiment comprovacioRendimentTmp) {
		for (Waypoint_Dades element : comprovacioRendimentTmp.llistaWaypoints) {
			System.out.println("id " + element.getId() + ": " + element.getNom() + ", data alta: " + element.getDataCreacio());
		}
	}
}
