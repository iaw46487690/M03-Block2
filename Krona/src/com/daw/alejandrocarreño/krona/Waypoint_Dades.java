package com.daw.alejandrocarreño.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Deque;

public class Waypoint_Dades implements Comparable<Waypoint_Dades> {
	private int id;
	private String nom;
	private int[] coordenades;
	private boolean actiu;
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;
	private LocalDateTime dataModificacio;
	private int tipus;
	
	/**
	 * @param id
	 * @param nom
	 * @param coordenades
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio, int tipus) {
		super();
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
		this.tipus = tipus;
	}

	// Getters i Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int[] getCoordenades() {
		return coordenades;
	}

	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}

	public String getCoordenadesString() {
		return coordenades[0] + " " + coordenades[1] + " " + coordenades[2];
	}

	@Override
	public String toString() {
		return "WAYPOINT " + id + ":\n\tnom = " + nom + "\n\tcoordenades(x, y, z) = " + Arrays.toString(coordenades)
				+ "\n\tactiu = " + actiu + "\n\tdataCreacio = " + dataCreacio + "\n\tdataAnulacio = " + dataAnulacio
				+ "\n\tdataModificacio = " + dataModificacio;
	}

	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public int getTipus() {
		return tipus;
	}

	public void setTipus(int tipus) {
		this.tipus = tipus;
	}

	@Override
	public int compareTo(Waypoint_Dades waypoint) {
		int c1 = this.coordenades[0] * this.coordenades[1] * this.coordenades[2];
		int c2 = waypoint.coordenades[0] * waypoint.coordenades[1] * waypoint.coordenades[2];
		int result = c1 - c2;
		if (result == 0) {
			result = this.getNom().compareTo(waypoint.getNom());
		}
		return result;
	}
}
