package Exercici1;

import java.util.Scanner;

public class Exercici1Main {

	public static void main(String[] args) {
		int array[] = {10,20,30};
		System.out.println("---- Entra un index del array longitud = 3 ----");
		Scanner sc = new Scanner(System.in);
		System.out.print("Index: ");
  	   	int index = sc.nextInt();
  	   	try {
   			int valor = array[index];
   			System.out.println(valor);
   			System.out.println("Final del programa!");
   			throw new ArrayIndexOutOfBoundsException();
  	   	} catch (Exception e) {
  	   		System.out.println("Codi del catch");
  	   		System.out.println(e);
  	   	} finally {
  	   		System.out.println("Final del programa!");
  	   	}
	}

}
