package Exercici5;

public class Exercici5ValidarEdatException extends Exception {
	@Override
	public String toString() {
		return ("Error. Has introduit una edat negativa o major a 100");
	}
}
