package Exercici4;

import java.util.Scanner;

public class PersonaMain {

	public static void main(String[] args) {
		Persona p = new Persona();
		System.out.println("---- Entra l'edat de la persona ----");
		Scanner sc = new Scanner(System.in);
		System.out.print("Edat: ");
  	   	int edat = sc.nextInt();
  	   	try {
  	   		if (edat < 0){
  	   			throw new IllegalArgumentException();
  	   		} else {
  	   			p.setEdat(edat);
  	   		}
  	   	} catch (Exception e) {
  	   		System.out.println(e);
  	   	}
	}

}
