package Exercici0;

import java.util.Scanner;

public class Exercici01Main {

	public static void main(String[] args) {
		System.out.println("----Entra un id----");
		Scanner sc = new Scanner(System.in);
		System.out.print("Id: ");
  	   	int user_id=sc.nextInt();
  	   	
  	   	try {
  	   		if (user_id!=1234){
  	   			throw new InvalidUserIdException();
  	   		}
  	   	} catch (Exception e){
  	   		System.out.println(e);
  	   	}
	}
}
