package Exercici3;

import java.util.Scanner;

public class DivisioMain {

	public static void main(String[] args) {
		System.out.println("---- Entra un numero ----");
		Scanner sc = new Scanner(System.in);
		System.out.print("Divisor: ");
  	   	int divisor = sc.nextInt();
  	   	try {
  	   		if (divisor == 0){
  	   			throw new Divisio();
  	   		}
  	   	} catch (Exception e) {
  	   		System.out.println(e);
  	   	}
	}

}
