package fitxers_examen.exercici1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class Exercici1 {

	static String pathDirArrel = "fitxers_examen/";
	static String pathDirExercici1 = "exercici1/";
	static String nomDirPatata = "patata";
	static String pathDirACopiar = "dirACopiar";

	private static void copiarNomEnFitxer(String nomFileTrobat, int tipusFile, StringBuilder permisosFile, PrintWriter pw) {
		
	}
	public static void main(String[] args) throws IOException {
		File directoriArrel = new File(pathDirArrel);
		File directoriPatata = new File(pathDirArrel + pathDirExercici1 + nomDirPatata);
		File directoriACopiar = new File(pathDirArrel + pathDirACopiar);
		File fitxerIndex = new File(pathDirArrel + pathDirExercici1 + nomDirPatata + "/index.txt");
		boolean creat;
		FileWriter fw = null;
		PrintWriter pw = null;
		StringBuilder permisosFile = new StringBuilder("---, ");
		File fitxerIndex2;
		// 1
		System.out.println("APARTAT 1");
		File directori = new File("src/fitxers_examen/exercici1/patata");
		if (directori.exists()) {
			System.out.println("El directori existeix");        
		} else {            
			System.out.println("El directori NO existeix, el creem");
			directori.mkdir();        
		}
		// 2
		System.out.println("APARTAT 2");
		File index = new File("src/fitxers_examen/exercici1/patata/index.txt");
		index.createNewFile();
		// 3
		System.out.println("APARTAT 3");
		if (index.exists()) {            
			System.out.println("El fitxer " + index.getName() + " existeix");        
		} else {            
			boolean exitCreacio = false;            
			System.out.println("El fitxer NO existeix, el creem");            
			index.createNewFile();            
			System.out.println("El fitxer s'ha creat? " + exitCreacio);        
		}
		// 4
		System.out.println("APARTAT 4");
		String linia1 = "Aquesta és la linia 1 del fitxer index.txt";
		String linia2 = "Aquesta és la linia 2 del fitxer index.txt";
		List<String> linies = Arrays.asList(linia1, linia2);
		FileUtils.writeLines(index, linies);
		System.out.println("S'han introduit 2 linies al fitxer " + index.getName());
		// 5
		System.out.println("APARTAT 5");
		File dirACopiar = new File("src/fitxers_examen/dirACopiar");
		FileUtils.copyDirectoryToDirectory(dirACopiar, directori);
		System.out.println("S'ha copiat el directori " + dirACopiar.getName() + " al directori " + directori.getName());
		// 6
		System.out.println("APARTAT 6");
		FileUtils.copyFileToDirectory(index, dirACopiar);
		System.out.println("S'ha copiat el fitxer " + index.getName() + " al directori " + dirACopiar.getName());
		// 7 i 8
		System.out.println("APARTATS 7 i 8");
		List<File> files = Arrays.asList(directori.listFiles());
		/*for (File file : files) {
			boolean append = true;
			fw = new FileWriter(fitxerIndex, append);
			pw = new PrintWriter(fw, true);
			Iterator it = FileUtils.iterateFilesAndDirs(directoriPatata, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			while (it.hasNext()) {
			File fileTmp = (File) it.next();
			System.out.println(fileTmp.getName());
			if (fileTmp.canRead())
			permisosFile.setCharAt(0, 'r');
			if (fileTmp.canWrite())
			permisosFile.setCharAt(1, 'w');
			if (fileTmp.canExecute())
			permisosFile.setCharAt(2, 'x');
			if (fileTmp.isHidden()){
			permisosFile.append("OCULT, ");
			}
			permisosFile.append("tamany = ");
			permisosFile.append(FileUtils.sizeOf(fileTmp));
			permisosFile.append(" bytes.");
			if (fileTmp.isDirectory()) {
			copiarNomEnFitxer(fileTmp.getName(), 0,
			permisosFile, pw);
			} else {
			copiarNomEnFitxer(fileTmp.getName(), 1,
			permisosFile, pw);
			}
			permisosFile = new StringBuilder("---, ");
			}
		}*/		
		// 9
		System.out.println("APARTAT 9");
		FileUtils.cleanDirectory(directori);
		System.out.println("S'ha esborrat el contingut del directori " + directori.getName());
		// 10
		System.out.println("APARTAT 10");
		File index2 = FileUtils.getFile("src/fitxers_examen/dirACopiar/index.txt");
		FileUtils.copyFileToDirectory(index2, directori);
		System.out.println("S'ha copiat el fitxer " + index2.getName() + " al directori " + directori.getName());
		// 11
		System.out.println("APARTAT 11");
		if (FileUtils.contentEquals(index, index2)) {
			System.out.println("El contingut dels 2 fitxers són iguals");
		} else {
			System.out.println("El contingut dels 2 fitxers són diferents");
		}
	}

}
