
public class Cotxe implements Vehicle {
	String nom;
	double preu;
	String marca;
	
	/**
	 * @param nom
	 * @param preu
	 * @param marca
	 */
	public Cotxe(String nom, double preu, String marca) {
		this.nom = nom;
		this.preu = preu;
		this.marca = marca;
	}
	
	// GETTERS i SETTERS
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	@Override
	public void Drive() {
		System.out.println("Conduint el cotxe");
	}

	@Override
	public void Stop() {
		System.out.println("Parar el cotxe");
	}

	@Override
	public void StartEngine() {
		System.out.println("Engegar el cotxe");
	}

	@Override
	public void TurnLeft() {
		System.out.println("Girar a l'esquerra");
	}

	@Override
	public void GoToITV() {
		System.out.println("Anar a la ITV");
	}
}
