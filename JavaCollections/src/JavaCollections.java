import java.util.ArrayList;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {
		
		//1. Declaramos una variable de tipo array, un array de int
        int edades[];
        
        //2. Instanciamos el array de int
        edades = new int[3];
        
        //3. Inicializamos los valores de los elementos del array
        edades[0] = 30;
        edades[1] = 15;
        edades[2] = 20;
        
        //4.imprimimos los valores del array
        System.out.println("array enteros indice 0:" + edades[0]);
        System.out.println("array enteros indice 1:" + edades[1]);
        System.out.println("array enteros indice 2:" + edades[2]);
        //System.out.println("arreglo enteros indice 0:" + edades[3]);
        
        //1. Declarar e instanciar un array de tipos object
        Persona personas[] = new Persona[4];
        //2. Inicializar los valores del array
        personas[0] = new Persona("Juan");
        personas[2] = new Persona("Carla");
        
        System.out.println("array personas indice 0:" + personas[0]);
        System.out.println("array personas indice 1:" + personas[1]);
        System.out.println("array personas indice 2:" + personas[2]);
        System.out.println("array personas indice 3:" + personas[3]);
        //System.out.println("Array personas indice 4:" + personas[4]);
        
        ArrayList<String> l1 = new ArrayList<String>();
        
        l1.add("Gat");
        l1.add("Jaguar");
        
        System.out.println(l1.get(0));
        System.out.println(l1.get(1));
        // System.out.println(l1.get(2)); java.lang.IndexOutOfBoundsException:
        l1.set(0, "Gos");
        System.out.println(l1.get(0));
        l1.add("Elefant");
        l1.add("lynx");
        
        if (l1.contains("Gos")) {
        	 System.out.println("Gós és present");
        } else {
        	System.out.println("Gós no trobat");
        }
        System.out.println();
        System.out.println("Mostrar amb bucle for:");
        for (int i = 0; i < l1.size(); i++) {
        	System.out.println(l1.get(i));
        }
        System.out.println();
        for (String animal : l1) {
        	System.out.println(animal);
        }
        System.out.println();
        System.out.println("Mostrar amb Iterator:");
        Iterator i = l1.iterator();
        while (i.hasNext()) {
        	System.out.println(i.next());
        }
        
        System.out.println();
        System.out.println("Array[]");
        String s[] = new String[l1.size()];
        s=l1.toArray(s);
        for (int y = 0; y < s.length; y++) {
        	System.out.println(s[y]);
        }
        System.out.println("\nCLASS COTXE");
        Cotxe c1 = new Cotxe("Gasoil 3008", 33000, "Peugeot");
        Cotxe c2 = new Cotxe("Diesel Picasso", 18000, "Citroën");
        Cotxe c3 = new Cotxe("X5", 50000, "BMW");
        c1.Stop();
        c2.StartEngine();
        c3.Drive();
        
        ArrayList<Cotxe> c = new ArrayList<Cotxe>();
        c.add(c1);
        c.add(c2);
        c.add(c3);
        
        for (int k = 0; k < c.size(); k++) {
        	System.out.println(c.get(k).marca + " " + c.get(k).nom);
        }
        
        for (Cotxe cotxe : c) {
        	System.out.println(cotxe.nom + " " + cotxe.preu);
        }
	}
}
