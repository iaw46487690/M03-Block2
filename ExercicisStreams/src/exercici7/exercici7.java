package exercici7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class exercici7 {

	public static void main(String[] args) throws IOException {
		File file = new File("src/exercici7/Exercici7.java"); 		
		numCaracters(file);
	}

	public static void numCaracters(File file) throws IOException { 		
		String linea; 		
		int caractersTotals = 0; 		
		int repeticio; 		
		char lletra; 		
		HashMap<Character, Integer> repeticions = new HashMap<Character, Integer>(); 		
		FileReader fr = new FileReader(file); 		
		BufferedReader br = new BufferedReader(fr);
		while ((linea = br.readLine()) != null) { 			
			for (int i = 0; i < linea.length(); i++) {			
				lletra = linea.charAt(i);			
				if (repeticions.get(lletra) == null) { 					
					repeticio = 0; 				
				} else { 					
					repeticio = repeticions.get(lletra); 				
				} 				
				repeticions.put(lletra, repeticio + 1); 			
			} 			
			caractersTotals += linea.length(); 		
		}
		fr.close(); 	
	}
}
