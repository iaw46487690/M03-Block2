package exercici9;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class exercicis9 {

	public static void main(String[] args) {
		String direccio = "src/exercici9/";
		File directori = new File(direccio);
		List<String> l1 = new ArrayList<String>();
		mostrarFitxers(directori, l1);
		System.out.println(l1);
	}
	public static void mostrarFitxers(File directori, List<String> l1) {
		String direccio = directori.getPath();
		String[] fitxers = directori.list();
		for (int i = 0; i < fitxers.length; i++) {
			File f = new File(direccio + "/" + fitxers[i]);
			if (f.isDirectory()) {
				mostrarFitxers(f, l1);
			} else {
				l1.add(fitxers[i]);
			}
		}
	}
}
