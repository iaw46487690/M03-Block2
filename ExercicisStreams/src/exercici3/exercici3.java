package exercici3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class exercici3 {

	public static void main(String[] args) throws IOException {
		File f1 = new File("text1.txt");
		File f2 = new File("text2.txt");
		FileReader fr1 = null;
		FileReader fr2 = null;
		BufferedReader br1;
		BufferedReader br2;
		String s1;
		String s2;
		
		try {
			fr1 = new FileReader(f1);
			fr2 = new FileReader(f2);
			br1 = new BufferedReader(fr1);
			br2 = new BufferedReader(fr2);
			s1 = br1.readLine();
			s2 = br2.readLine();
			int contador = 1;
			while(s1 != null && s2 != null && s1.equals(s2)) {
				s1 = br1.readLine();
				s2 = br2.readLine();
				contador++;
			}
			if (s1 != null || s2 != null) {
				System.out.println("Error en la linea: " + contador);
				System.out.println("Linea al primer arxiu " + s1);
				System.out.println("Linea al segon arxiu: " + s2);
			}
		} catch(FileNotFoundException e) {
			System.out.println("El arxiu no existeix");
		} catch(IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr1 != null) fr1.close();
			if (fr2 != null) fr2.close();
		}
	}

}
