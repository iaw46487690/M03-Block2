package exercici4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class exercici4 {

	public static void main(String[] args) throws IOException {
		File directory = new File("src/exercici4/");
        File[] fileDirList = directory.listFiles();
        String backupDirectoryPath = createBackupName(directory);
        copyFilesIntoDirectory(fileDirList, backupDirectoryPath);
	}
	
	 public static String createBackupName(File directory) {
        int backupNumber = 0;
        File backupDirectory = new File(directory.getAbsolutePath() + "/" + "backUp" + backupNumber);
        while (backupDirectory.exists()) {
            backupNumber++;
            backupDirectory.renameTo(new File(directory.getAbsolutePath() + "/" + "backUp" + backupNumber));
        }
        backupDirectory.mkdir();
        return backupDirectory.getAbsolutePath();
    }
	 
	 public static void copyFilesIntoDirectory(File[] fileDirList, String directoryPath) throws IOException {
        String line;
        for (File file : fileDirList) {
            if (file.canRead()) {
                if (file.isFile()) {
                    File backupFile = new File(directoryPath + "/" + file.getName());
                    FileReader fr = new FileReader(file);
                    BufferedReader br = new BufferedReader(fr);
                    FileWriter fw = new FileWriter(backupFile, true);
                    PrintWriter pw = new PrintWriter(fw, true);
                    while ((line = br.readLine()) != null) {
                        pw.println(line);
                    }
                    fr.close();
                    fw.close();
                    backupFile.createNewFile();
                }
            } else {
                System.out.println("El fitxer " + file.getAbsolutePath() + " no es pot llegir");
            }
        }
    }
}
