package exercici5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class exercici5 {

	public static void main(String[] args) {
		try {
			File fitxer = new File("file.txt");
			File[] fitxers = fitxer.listFiles();
			numeroLineas(fitxers);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void numeroLineas(File[] llistaArxius) throws IOException {
		for (File fitxer: llistaArxius) {
			FileReader fr = null;
			BufferedReader br;
			String linea;
			int numLineas = 0;
			int numCaracters = 0;
			if (fitxer.isFile()) {
				try {
					fr = new FileReader(fitxer);
					br = new BufferedReader(fr);
					while((linea = br.readLine()) != null) {
						numLineas++;
						numCaracters += linea.length();
					}
					System.out.println("El fitxer té " + numLineas + " lineas i " + numCaracters + " caracters");
				} catch (FileNotFoundException e) {
					System.out.println("El fitxer no existeix");
				} catch (IOException e) {
					System.out.println(e.getMessage());
				} finally {
					if (fr != null) {
						fr.close();
					}
				}
			}
			
		}
	}

}
