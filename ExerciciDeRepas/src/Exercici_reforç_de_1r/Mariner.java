package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Mariner extends Tripulant {
	
	public boolean serveiEnElPont;
	public String descripcioFeina;
	
	public Mariner(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei, boolean serveiEnElPont, String descripcioFeina) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}
	
	public String serveixEnElPont() {
		String serveix;
		if (serveiEnElPont == true) {
			serveix = "Si";
		} else {
			serveix = "No";
		}
		return serveix;
	}

	@Override
	public void ImprimirDadesTripulant() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		System.out.println("Bandol: " + bandol);
		System.out.println("ID: " + this.ID);
		System.out.println("Nom: " + this.nom);
		System.out.println("Actiu: " + this.actiu);
		System.out.println("Departament (de la classe Tripulant): " + this.departament);
		System.out.println("Departament (de la classe IKSRotarranConstants): " + IKSRotarranConstants.DEPARTAMENTS[this.departament]);
		System.out.println("Lloc en el que serveix (de la classe Tripulant): " + this.getLlocDeServei());
		System.out.println("Lloc en el que serveix (de la classe IKSRotarranConstants): " + IKSRotarranConstants.LLOCS_DE_SERVEI[this.getLlocDeServei()]);
		System.out.println("Descripció de la feina que fa: " + this.descripcioFeina);
		System.out.println("Serveix en el pont: " + this.serveixEnElPont());
		System.out.println("Data d'alta: " + this.dataAlta.format(formatter));
	}

}
