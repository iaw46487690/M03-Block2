package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Timoner extends Tripulant {
	
	public boolean serveiEnElPont;
	public String descripcioFeina;
	
	public Timoner(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}
	
	public String serveixEnElPont() {
		String serveix;
		if (serveiEnElPont == true) {
			serveix = "Si";
		} else {
			serveix = "No";
		}
		return serveix;
	}

	@Override
	public void ImprimirDadesTripulant() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		System.out.println("Bandol: " + bandol);
		System.out.println("Id: " + this.ID);
		System.out.println("Nom: " + this.nom);
		System.out.println("Esta actiu: " + this.actiu);
		System.out.println("Data d'alta: " + dataAlta.format(formatter));
		System.out.println("Departament: " + this.departament);
		System.out.println("Lloc en el que serveix: " + this.getLlocDeServei());
		System.out.println("Serveix en el pont: " + this.serveixEnElPont());
		System.out.println("Descripció de la feina que fa: " + this.descripcioFeina);
	}
	
	@Override
	public String toString() {
		return "Oficial [ID=" + ID + ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament="
				+ departament + ", serveiEnElPont=" + serveiEnElPont + ", descripcioFeina=" + descripcioFeina + "]";
	}
}
