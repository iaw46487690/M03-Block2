package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public abstract class Tripulant {
	
	// ATRIBUTS
	protected static final String bandol = "Imperi Klingon";
	protected String ID;
	protected String nom;
	protected boolean actiu;
	protected LocalDateTime dataAlta;
	protected int departament;
	private int llocDeServei;
	
	// CONSTRUCTOR
	public Tripulant(String iD, String nom, boolean actiu, LocalDateTime dataAlta2, int departament, int llocDeServei) {
		this.ID = iD;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta2;
		this.departament = departament;
		this.setLlocDeServei(llocDeServei);
	}

	// GETTERS i SETTERS
	public int getDepartament() {
		return departament;
	}

	public void setDepartament(int departament) {
		this.departament = departament;
	}
	
	public int getLlocDeServei() {
		return llocDeServei;
	}

	public void setLlocDeServei(int llocDeServei) {
		this.llocDeServei = llocDeServei;
	}
	
	// METODES
	public abstract void ImprimirDadesTripulant();
	
	public void saludar() {
		System.out.println("Hola des de la superclasse Tripulant");
	}
	
	// hashCode i equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}
	
	// toString
	@Override
	public String toString() {
		return "Tripulant [ID=" + ID + ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament="
				+ departament + ", llocDeServei=" + llocDeServei + "]";
	}
}
