package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IKSRotarran {

	public static void main(String[] args) {
		System.out.println("EXERCICI 2");
		System.out.println("----------");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		Oficial capita = new Oficial("001-A", "Martok", true, LocalDateTime.parse("15-08-1954 00:01", formatter), 1, 1, true, "Capitanejar la nau.");
		Mariner mariner02_03 = new Mariner("758-J", "Kurak", true, LocalDateTime.parse("26-12-1981 13:42", formatter), 3, 1, true, 
				"Mariner encarregat del timó i la navegació durant el 2ntorn.");
		System.out.println("Departament capita" + capita.departament);
		System.out.println("Descripcio feina capita" + capita.descripcioFeina);
		capita.ImprimirDadesTripulant();
		capita.setDepartament(10);
		System.out.println("Nou departament del capità: " + capita.getDepartament());
		System.out.println();
		
		System.out.println("EXERCICI 3");
		System.out.println("----------");
		Tripulant oficialDeTipusTripulant = new Oficial("001-A", "Martok", true, LocalDateTime.parse("15-08-1954 00:01", formatter), 1, 1, false, null);
		Oficial oficialDeTipusOficial = new Oficial("002-A", "Oficial002-B", true, null, 1, 1, false, null);
		oficialDeTipusTripulant.saludar();
		oficialDeTipusOficial.saludar();
		System.out.println();
		
		System.out.println("EXERCICI 5");
		System.out.println("----------");
		System.out.println("Lloc de servei: " + IKSRotarranConstants.LLOCS_DE_SERVEI[capita.getLlocDeServei()]);
		System.out.println();
		
		System.out.println("EXERCICI 6");
		System.out.println("----------");
		System.out.println("L'objecte capita (té implementat el toString()): " + capita);
		System.out.println("L'objecte mariner02_03 (NO té implementat el toString()): " + mariner02_03);
		System.out.println();
		
		System.out.println("EXERCICI 7");
		System.out.println("----------");
		System.out.println(mariner02_03);
		mariner02_03.ImprimirDadesTripulant();
		
	}

}
