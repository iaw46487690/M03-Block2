package exercicisList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Exercicis_1_2 {

	public static void main(String[] args) {
		
		System.out.println("-----Exercici 1-----");
		List<Integer> numeros = new ArrayList<Integer>();
		
		for (int i = 0; i < args.length; i++) {
			numeros.add(Integer.parseInt(args[i]));
		}
		System.out.println("Nombres d'elements a la llista: " + numeros.size());
		
		Integer cuadrat;
		for (int i = 0; i < numeros.size(); i++) {
			cuadrat = (int) Math.pow(numeros.get(i), 2);
			System.out.println(numeros.get(i) + " -> " + cuadrat);
			numeros.set(i, cuadrat);
		}
		
		Iterator<Integer> nums = numeros.iterator();
		while (nums.hasNext()) {
			if (nums.next() > 100) {
				nums.remove();
			}
		}
		
		System.out.println();
		Collections.sort(numeros);
		for (Integer num : numeros) {
			System.out.println(num);
		}
		
		System.out.println();
		System.out.println("-----Exercici 2-----");
		List<Integer> numeros2 = new ArrayList<Integer>();
		numeros2.add(5);
		numeros2.add(75);
		numeros2.add(42);
		numeros2.add(27);
		numeros2.add(14);
		numeros.addAll(numeros2);
		for (int num : numeros2) {
			if (numeros.contains(num)) {
				System.out.println("El numero " + num + " de la segona llista esta a la primera");
			}
		}
		numeros2.clear();
		if (numeros2.isEmpty()) {
			System.out.println("La llista numeros2 esta buida");
		}
	}
}
