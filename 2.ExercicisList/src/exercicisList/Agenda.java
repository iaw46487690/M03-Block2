package exercicisList;

public class Agenda implements Comparable<Agenda>{
	
	private String nom;
	private String cognom1;
	private String cognom2;
	private int telefon;
	private String email;
	private int mobil;
	
	// CONSTRUCTOR
	public Agenda(String nom, String cognom1, String cognom2, int telefon, String email, int mobil) {
		super();
		this.nom = nom;
		this.cognom1 = cognom1;
		this.cognom2 = cognom2;
		this.telefon = telefon;
		this.email = email;
		this.mobil = mobil;
	}

	// GETTERS I SETTERS
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom1() {
		return cognom1;
	}

	public void setCognom1(String cognom1) {
		this.cognom1 = cognom1;
	}

	public String getCognom2() {
		return cognom2;
	}

	public void setCognom2(String cognom2) {
		this.cognom2 = cognom2;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getMobil() {
		return mobil;
	}

	public void setMobil(int mobil) {
		this.mobil = mobil;
	}

	@Override
	public int compareTo(Agenda agenda) {
		int result = this.getCognom1().compareTo(agenda.getCognom1());
		if (result == 0) {
			result = this.getCognom2().compareTo(agenda.getCognom2());
		}
		if (result == 0) {
			result = this.getNom().compareTo(agenda.getNom());
		}
		return result;
	}

	@Override
	public String toString() {
		return nom + " " + cognom1 + " " + cognom2;
	}
}
