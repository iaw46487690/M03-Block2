package exercicisList;

import java.util.Date;

public class Persona implements Comparable<Persona>{
	
	private String nom;
	private String cognoms;
	private Date naixement;
	private String dni;
    private int edat;
	
    // CONSTRUCTOR
	public Persona(String nom, String cognoms, Date naixement, String dni, int edat) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.naixement = naixement;
		this.dni = dni;
		this.edat = edat;
	}

	// GETTERS I SETTERS
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public Date getNaixement() {
		return naixement;
	}

	public void setNaixement(Date naixement) {
		this.naixement = naixement;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}


	@Override
	public int compareTo(Persona persona) {
		int result = this.getEdat() - persona.getEdat();
		if (result == 0) {
			result = this.getCognoms().compareTo(persona.getCognoms());
		}
		return result;
	}    
}
