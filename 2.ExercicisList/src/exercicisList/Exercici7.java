package exercicisList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercici7 {

	public static void main(String[] args) {
		List<String> baralla = new ArrayList<String>();
		String palo[] = {"corazon", "picas", "diamantes", "trevoles"};
		String numero[] = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
		String carta;
		for (int i = 0; i < palo.length; i++) {
			for (int y = 0; y < numero.length; y++) {
				carta = numero[y] + " " + palo[i];
				baralla.add(carta);
			}
		}
		Collections.shuffle(baralla);
		
		
		repartirJugadors(baralla, 2, 7);
		System.out.println("La baralla restant: ");
		//System.out.println(baralla.size());
		for (String card : baralla) {
			System.out.println(card);
		}
	
	}
	
	public static List<String> repartir(List<String> baralla, int n) {
		List<String> sublist = baralla.subList(baralla.size() - n, baralla.size());
		List<String> ma = new ArrayList<String>(sublist);
		baralla.subList(baralla.size() - n, baralla.size()).clear();
		return ma;
	}
	
	public static void repartirJugadors(List<String> baralla, int jugadors, int cartes) {
		if (jugadors * cartes > baralla.size()) {
			System.out.println("Not enought cards.");
			return;
		}
		for (int i = 0; i < jugadors; i++) {
			System.out.println("----Cartes del jugador " + i);
			System.out.println(repartir(baralla, cartes));
		}
	}	
}
