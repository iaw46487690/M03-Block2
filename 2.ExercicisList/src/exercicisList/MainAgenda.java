package exercicisList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainAgenda {

	public static void main(String[] args) {
		Agenda ag1 = new Agenda("Alejandro", "Carreño", "Rubio", 12345678, "alejandro.cr473@gmail.com", 12345678);
		Agenda ag2 = new Agenda("Alejandro", "Garcia", "Fernandez", 12345678, null, 12345678);
		Agenda ag3 = new Agenda("Raul", "Garcia", "Fernandez", 12345678, null, 12345678);
		Agenda ag4 = new Agenda("Marta", "Garcia", "Rubio", 12345678, null, 12345678);
		
		List<Agenda> agendes = new ArrayList<Agenda>();
		agendes.add(ag1);
		agendes.add(ag2);
		agendes.add(ag3);
		agendes.add(ag4);
		
		Collections.sort(agendes);
		
		for (int i = 0; i < agendes.size(); i++) {
			System.out.println(agendes.get(i).toString());
		}
	}
}
