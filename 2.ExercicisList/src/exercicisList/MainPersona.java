package exercicisList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MainPersona {

	public static void main(String[] args) {
		Persona per1 = new Persona("Alejandro", "Carreño Rubio", new Date(100, 11, 3), "46487690K", 20);
		Persona per2 = new Persona("Sergio", "Rodriguez", new Date(107, 11, 3), "12345678J", 13);
		Persona per3 = new Persona("Pol", "Garcia", new Date(103, 11, 3), "45678324L", 17);
		Persona per4 = new Persona("Daniel", "Martins", new Date(103, 11, 3), "12457890P", 17);

		List<Persona> persones = new ArrayList<Persona>();
		persones.add(per1);
		persones.add(per2);
		persones.add(per3);
		persones.add(per4);
		
		List<Persona> menorsDe18 = new ArrayList<Persona>();
		String name;
		for (int i = 0; i < persones.size(); i++) {
			if (persones.get(i).getEdat() < 18) {
				menorsDe18.add(persones.get(i));
			}
		}
		System.out.println("-----Exercici 3-----");
		for (int i = 0; i < menorsDe18.size(); i++) {
			System.out.println(menorsDe18.get(i).getNom() + " " + menorsDe18.get(i).getCognoms() + " | Edat: " + menorsDe18.get(i).getEdat());
		}
		
		System.out.println();
		System.out.println("-----Exercici 4-----");
		Collections.sort(persones);
		int i = 0;
		while (persones.get(i).getEdat() < 18) {
			System.out.println(persones.get(i).getNom() + " " + persones.get(i).getCognoms() + " | Edat: " + persones.get(i).getEdat());
			i++;
		}
		
		System.out.println();
		System.out.println("-----Exercici 5-----");
		Collections.sort(persones);
		for (int k = 0; k < persones.size(); k++) {
			System.out.println(persones.get(k).getNom() + " " + persones.get(k).getCognoms() + " | Edat: " + persones.get(k).getEdat());
		}
	}
}